




''' dictionary connected to the below and we can see that after each number' ' we have place holder
    when selecting a number we fill the comas with our

'''

theBoard =  { '13':' ','14':' ','15':' ','16': ' ',
              '9': ' ','10':' ','11':' ','12': ' ',
              '5': ' ','6': ' ','7': ' ','8' : ' ',
              '1': ' ','2': ' ','3': ' ','4' : ' '}

#tried to add collors unsuccesfuly

class theboard:
   PURPLE = 'x'=='\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


board_keys = []
#keys are expected to be int but in the square brackets I could insert other brackets and all was working

for key in theBoard:
    board_keys.append(key)

# printboard is connected to the dictionary  called "theboard"
#Tried to create a board UI

def create_board_ui():
    global lbl_status, top_frame, list_labels
    top_frame = tk.Frame(window_main)
    for x in range(4):4
    for y in range(4):
            lbl = tk.Label(top_frame, text=" ", font="Helvetica 45 bold", height=2, width=5, highlightbackground="grey",
                           highlightcolor="grey", highlightthickness=1)
            lbl.bind("<Button-1>", lambda e, xy=[x, y]: get_coordinate(xy))
            lbl.grid(row=x, column=y)

            dict_labels = {"xy": [x, y], "symbol": "", "label": lbl, "ticked": False}
            list_labels.append(dict_labels)

    lbl_status = tk.Label(top_frame, text="Status: Not connected to server", font="Helvetica 14 bold")
    lbl_status.grid(row=3, columnspan=3)

    top_frame.pack_forget()



#printboard (web assisted)
def printBoard(board):
    print(board['13'] + ' | ' + board['14']+ ' | ' + board['15']+ ' | ' + board['16'])
    print('-------------')
    print(board['9'] +  ' | ' + board['10'] + ' | ' + board['11']+ ' | ' + board['12'])
    print('-------------')
    print(board['5'] + ' | ' + board['6'] + ' | ' + board['7']+ ' | ' + board['8'])
    print('-------------')
    print(board['1'] + ' | ' + board['2'] + ' | ' + board['3']+ ' | ' + board['4'])


# Game play : a lot of web assistance but I have altered and replicated the below

def game():
    turn = 'x'.format(classmethod)
    count = 0

    for i in range(17):
        printBoard(theBoard)
        print("Your turn," + turn + ".Move Where?")

        move = input()

        if theBoard[move] == ' ':
            theBoard[move] = turn
            count += 1
        else:
            print("That place is already filled.\nMove to which place?")
            continue

        # Game conditions.
        if count >= 5:

            if theBoard['13'] == theBoard['14'] == theBoard['15'] == theBoard['16']!= ' ':  # across the top
                printBoard(theBoard)
                print("\nGame Over.\n")
                print(" **** " + turn + " won. ****")
                break

            elif theBoard['9'] == theBoard['10'] == theBoard['11'] == theBoard['12']!= ' ':  # across the middle2
                printBoard(theBoard)
                print("\nGame Over.\n")
                print(" **** " + turn + " won. ****")
                break

            elif theBoard['5'] == theBoard['6'] == theBoard['7'] == theBoard['8']!= ' ':  # across the middle
                printBoard(theBoard)
                print("\nGame Over.\n")
                print(" **** " + turn + " won. ****")
                break
            elif theBoard['1'] == theBoard['2'] == theBoard['3'] == theBoard['4']!= ' ':  # across the bottom
                printBoard(theBoard)
                print("\nGame Over.\n")
                print(" **** " + turn + " won. ****")
                break
            elif theBoard['1'] == theBoard['5'] == theBoard['9'] == theBoard['13']!= ' ':  # down the left side
                printBoard(theBoard)
                print("\nGame Over.\n")
                print(" **** " + turn + " won. ****")
                break
            elif theBoard['2'] == theBoard['6'] == theBoard['10'] == theBoard['14']!= ' ':  # down the middle
                printBoard(theBoard)
                print("\nGame Over.\n")
                print(" **** " + turn + " won. ****")
                break
            elif theBoard['4'] == theBoard['7'] == theBoard['10'] == theBoard['13']!= ' ':  # down the right side
                printBoard(theBoard)
                print("\nGame Over.\n")
                print(" **** " + turn + " won. ****")
                break
            elif theBoard['3'] == theBoard['5'] == theBoard['7'] == theBoard['11']!= ' ':  # diagonal
                printBoard(theBoard)
                print("\nGame Over.\n")
                print(" **** " + turn + " won. ****")
                break
            elif theBoard['1'] == theBoard['6'] == theBoard['11'] == theBoard['16'] != ' ':  # diagonal
                printBoard(theBoard)
                print("\nGame Over.\n")
                print(" **** " + turn + " won. ****")
                break

                # Is Tie ?
        if count == 30:
            print("\nGame Over.\n")
            print("It's a Tie!!")

        # Player switch
        if turn == 'x':
            turn = 'y'
        else:
            turn = 'x'

            # replay option.
    restart = input("Do want to play Again?(y/n)")
    if restart == "y" or restart == "Y":
        for key in board_keys:
            theBoard[key] = " "

        game()
if __name__ == "__main__":
    game()